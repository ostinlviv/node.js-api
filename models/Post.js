const mongoose = require('mongoose');
const slugify = require('slugify');

const PostSchema = new mongoose.Schema(
  {
    title: {
      type: String,
      required: [true, 'Please add a title'],
      unique: true,
      trim: true,
      maxlength: [50, 'Title can not be more than 50 characters']
    },
    slug: String,
    content: {
      type: String,
      required: [true, 'Please add a content'],
      maxlength: [500, 'Content can not be more than 500 characters']
    },
    photo: {
      type: String,
      default: 'no-photo.jpg'
    },
    createdAt: {
      type: Date,
      default: Date.now
    },
    user: {
        type: mongoose.Schema.ObjectId,
        ref: 'User',
        required: true
    }
  }
);

PostSchema.pre('save', function(next) {
  this.slug = slugify(this.title, { lower: true });
  next();
});

PostSchema.pre('remove', async function(next) {
  await this.model('Comment').deleteMany({ post: this._id });
  next();
});

module.exports = mongoose.model('Post', PostSchema);