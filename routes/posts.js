const express = require('express');
const {
    getPosts,
    getPost,
    createPost,
    updatePost,
    deletePost,
    postPhotoUpload
} = require('../controllers/posts');

const Post = require('../models/Post');

const commentRouter = require('./comments');

const router = express.Router();

const advancedResults = require('../middleware/advancedResults');
const { protect, authorize } = require('../middleware/auth');

router.use('/:postId/comments', commentRouter);

router
    .route('/')
    .get(advancedResults(Post, 'comments'), getPosts)
    .post(protect, authorize('publisher', 'admin'), createPost);

router
    .route('/:id')
    .get(getPost)
    .put(protect, authorize('publisher', 'admin'), updatePost)
    .delete(protect, authorize('publisher', 'admin'), deletePost);

router
    .route('/:id/photo')
    .put(protect, authorize('publisher', 'admin'), postPhotoUpload);

module.exports = router;